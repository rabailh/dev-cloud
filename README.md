## Le projet

Ce projet à pour but le développement d'une application NextJs mettant à disposition une api locale qui interagit avec une base de données cloud.

On retrouve alors :
- une application NextJs utilisant du javascript et du typescipt
- une base de données MongoDb hébergée par Atlas contenant des données de la base "mflix" (proposée par Atlas)
- un description de l'api en swagger

## Lancement du projet en local

Prérequis :
- Node.js
- Git bash

Pour installer et lancer l'application sur un environnement local, il faut exécuter les commandes suivantes dans un terminal ;

- Récuperer le projet et s'y rendre
```bash
git clone https://gitlab.com/rabailh/dev-cloud.git && cd dev-cloud/
```


- Installer les dépendances
```bash
npm i
```

- Lancer l'application 
```bash
npm run dev
```

Si tout s'est bien déroulé, vous devriez pouvoir ouvrir votre navigateur sur l'url http://localhost:3000/ et voir le résultat.

## Utilisation du swagger fourni

Si l'application est en route, alors il est possible d'accéder au swagger décrivant l'api.

Pour cela il suffit d'accéder à http://localhost:3000/swagger

Après un cours temps de chargement on retrouve la liste des endpoints référencés. Cela se présente sous forme de liste d'endpoints déroulant. En cliquant dessus, les informations supplémentaires telles qu'un description, les paramètres d'entrée nécessaires, ou encore les code de status de l'appel (200, 400, 500, ...).

Il est possible d'utiliser cette interface pour tester les endpoints grâce au bouton "Try it out" qui apparaît à la sélection d'un des éléments.

Le liens tout en haut de la page /api/doc ouvre un onglet contenant les informations du swagger au format JSON.

## Tentative de développement d'interface

Un essai de développement d'interface à été tenté à http://localhost:3000/, dans l'idée de découvrir le développement en Nextjs. Bien que ce ne soit pas très fourni, c'était dans l'idée de progresser dans l'utilisation du framework et dans l'idée d'élargir le périmètre de ce que pouvais faire l'application.

L'interface est une simple page permettant l'utilisation des endpoints de /movies et /movie. Elle est divisée en deux composant, un pour chaque route d'endpoint (movies et movie), qui sont accessible de manière séparée avec 
http://localhost:3000/movies et http://localhost:3000/movie.

