import clientPromise from "/lib/mongodb";
import { Config } from "/services/ConfigService";

const connectToDb = async () => {
  const client = await clientPromise;
  return await client.db(Config.databases.mflix);
};

const db = await connectToDb();

const _find = async (dbName, optionalFields, page) => {
  const limit = 10
  const startIndex = (page - 1) * limit;
  return await db.collection(dbName).find(optionalFields).skip(startIndex).limit(limit).toArray();
};

const _findById = async (dbName, id) => {
  return await db.collection(dbName).findOne(id);
};

const _insert = async (dbName, object) => {
  return await db.collection(dbName).insertOne(object);
};

const _deleteById = async (dbName, id) => {
  return await db.collection(dbName).findOneAndDelete(id);
};

const _updateById = async (dbName, id, updateData) => {
  //update only not empty fields
  return await db.collection(dbName).findOneAndUpdate(id,{ $set: updateData });
};

export const OrmService = {
    find: _find,
    findById: _findById,
    insert: _insert,
    deleteById: _deleteById,
    updateById: _updateById
  };
