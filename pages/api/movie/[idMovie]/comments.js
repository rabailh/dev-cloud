import { ObjectId } from 'mongodb';
import {OrmService} from "/services/OrmService";
import {Config} from "/services/ConfigService";

/**
 * @swagger
 * /api/movie/{idMovie}/comments:
 *   post:
 *     summary: Add a comment
 *     description: Add a new comment to the specified movie
 *     parameters:
 *       - in: path
 *         name: idMovie
 *         required: true
 *         description: ID of the movie to which the comment will be added
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The name of the commenter
 *               email:
 *                 type: string
 *                 format: email
 *                 description: The email of the commenter
 *               movie_id:
 *                 type: string
 *                 description: The ID of the movie associated with the comment
 *               text:
 *                 type: string
 *                 description: The text content of the comment
 *               date:
 *                 type: string
 *                 format: date-time
 *                 description: The date and time the comment was made
 *             required:
 *               - name
 *               - movie_id
 *               - text
 *     responses:
 *       201:
 *         description: Comment added successfully
 *       400:
 *         description: Bad Request. The request body is missing required fields or contains invalid data.
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 *   get:
 *     summary: Get comments
 *     description: Retrieve comments associated with the specified movie
 *     parameters:
 *       - in: path
 *         name: idMovie
 *         required: true
 *         description: ID of the movie for which comments will be retrieved
 *         schema:
 *           type: string
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *         description: The page number to retrieve (default is 1).
 *     responses:
 *       200:
 *         description: Comments retrieved successfully
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 */
export default async function handler(req, res) {
    const { idMovie } = req.query;

    switch (req.method) {
        case "POST":
            const commentToInsert = req.body;
            if (!commentToInsert.text || !commentToInsert.name) {
                return res.json({ status: 400, error: "Bad Request", message: "Il manque au moins une information requise (text ou name)" });
            }
            if (commentToInsert._id || commentToInsert.movie_id) {
                return res.json({ status: 400, error: "Bad Request", message: "Les attributs _id et movie_id ne doivent pas être fournis" });
            }
            commentToInsert.movie_id = new ObjectId(idMovie)
            const result = await OrmService.insert(Config.collections.comments, commentToInsert)
            res.json({ status: 201, data: result});
            break;
        case "GET":
            let { page } = req.query;
            if(!page || parseInt(page) == 0) {
                page = 1
            }
            const comments = await OrmService.find(Config.collections.comments, { movie_id: new ObjectId(idMovie) }, page);
            res.json({ status: 200, data: comments, page: page });
            break;
        default :
            res.json({ status: 405, data: "Method not allowed" });
            break;
    }
}
