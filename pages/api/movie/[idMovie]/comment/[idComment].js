import { ObjectId } from 'mongodb';
import {OrmService} from "/services/OrmService";
import {Config} from "/services/ConfigService";

/**
 * @swagger
 * /api/comment/{idComment}:
 *   put:
 *     summary: Update a comment
 *     description: Update an existing comment in the database
 *     parameters:
 *       - in: path
 *         name: idComment
 *         required: true
 *         description: ID of the comment to update
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The new name of the commenter
 *               email:
 *                 type: string
 *                 format: email
 *                 description: The new email of the commenter
 *               text:
 *                 type: string
 *                 description: The new text content of the comment
 *             required:
 *               - name
 *               - text
 *     responses:
 *       200:
 *         description: Comment successfully updated
 *       400:
 *         description: Bad Request. The request body is missing required fields or contains invalid data.
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 *   delete:
 *     summary: Delete a comment
 *     description: Delete an existing comment from the database
 *     parameters:
 *       - in: path
 *         name: idComment
 *         required: true
 *         description: ID of the comment to delete
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Comment successfully deleted
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 *   get:
 *     summary: Get a comment
 *     description: Retrieve details of a specific comment from the database
 *     parameters:
 *       - in: path
 *         name: idComment
 *         required: true
 *         description: ID of the comment to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Comment details retrieved successfully
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 */

export default async function handler(req, res) {

    const { idComment } = req.query;

    switch (req.method) {
        case "PUT":
            const payload = req.body;
            if (!payload) {
                return res.json({ status: 400, error: "Bad Request", message: "No data provided for update" });
            }
            if(payload._id){
                return res.json({ status: 400, error: "Bad Request", message: "No id expected" });
            }
            const updatedMovie = await OrmService.updateById(Config.collections.comments, { _id: new ObjectId(idComment) }, payload);
            res.json({ status: 200, data: updatedMovie });
            break;
        case "DELETE":
            const deletedComment = await OrmService.deleteById(Config.collections.comments, { _id : new ObjectId(idComment) });
            res.json({ status: 200, data: deletedComment })
            break;
        case "GET":
            const dbComment = await OrmService.findById(Config.collections.comments, { _id : new ObjectId(idComment) });
            res.json({ status: 200, data: {comment: dbComment} });
            break;
        default :
            res.json({ status: 405, data: "Method not allowed" });
            break;
    } 

}