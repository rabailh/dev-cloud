import { ObjectId } from 'mongodb';
import {OrmService} from "/services/OrmService";
import {Config} from "/services/ConfigService";

/**
 * @swagger
 * /api/movie/{idMovie}:
 *   put:
 *     summary: Update a movie
 *     description: Update an existing movie in the database
 *     parameters:
 *       - in: path
 *         name: idMovie
 *         required: true
 *         description: ID of the movie to update
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               plot:
 *                 type: string
 *                 description: The new plot of the movie
 *               genres:
 *                 type: array
 *                 items:
 *                   type: string
 *                 description: The new genres of the movie
 *               runtime:
 *                 type: string
 *                 description: The new runtime of the movie
 *               cast:
 *                 type: array
 *                 items:
 *                   type: string
 *                 description: The new cast of the movie
 *               num_mflix_comments:
 *                 type: int
 *                 description: The new number of comments on mflix
 *               title:
 *                 type: string
 *                 description: The new title of the movie
 *               released:
 *                 type: string
 *                 description: The new release date of the movie
 *               directors:
 *                 type: array
 *                 items:
 *                   type: string
 *                 description: The new directors of the movie
 *               year:
 *                 type: string
 *                 description: The new year of the movie
 *     responses:
 *       200:
 *         description: Movie successfully updated
 *       400:
 *         description: Bad Request. The request body is missing required fields or contains invalid data.
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 *   delete:
 *     summary: Delete a movie
 *     description: Delete an existing movie from the database
 *     parameters:
 *       - in: path
 *         name: idMovie
 *         required: true
 *         description: ID of the movie to delete
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Movie successfully deleted
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 *   get:
 *     summary: Get a movie
 *     description: Retrieve details of a specific movie from the database
 *     parameters:
 *       - in: path
 *         name: idMovie
 *         required: true
 *         description: ID of the movie to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Movie details retrieved successfully
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 */

export default async function handler(req, res) {

    const { idMovie } = req.query;

    switch (req.method) {
        case "PUT":
            const payload = req.body;
            if (!payload) {
                return res.json({ status: 400, error: "Bad Request", message: "No data provided for update" });
            }
            if(payload._id){
                return res.json({ status: 400, error: "Bad Request", message: "No id expected" });
            }
            const updatedMovie = await OrmService.updateById(Config.collections.movies, { _id: new ObjectId(idMovie) }, payload);
            res.json({ status: 200, data: updatedMovie });
            break;
        case "DELETE":
            const deletedMovie = await OrmService.deleteById(Config.collections.movies, { _id : new ObjectId(idMovie) });
            res.json({ status: 200, data: deletedMovie })
            break;
        case "GET":
            const dbMovie = await OrmService.findById(Config.collections.movies, { _id : new ObjectId(idMovie) });
            res.json({ status: 200, data: {movie: dbMovie} });
            break;
        default :
            res.json({ status: 405, data: "Method not allowed" });
            break;
    } 

}