import {OrmService} from "/services/OrmService";
import {Config} from "/services/ConfigService";

/**
 * @swagger
 * /api/movies:
 *   get:
 *     summary: Returns a list of movies
 *     description: Returns a paginated list of movies.
 *     parameters:
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *         description: The page number to retrieve (default is 1).
 *     responses:
 *       200:
 *         description: Your list of movies in JSON
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 *   post:
 *     summary: Add a new movie
 *     description: Add a new movie to the database
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               plot:
 *                 type: string
 *                 description: The plot of the movie
 *               genres:
 *                 type: array
 *                 items:
 *                   type: string
 *                 description: The genres of the movie
 *               runtime:
 *                 type: string
 *                 description: The runtime of the movie
 *               cast:
 *                 type: array
 *                 items:
 *                   type: string
 *                 description: The cast of the movie
 *               num_mflix_comments:
 *                 type: int
 *                 description: The number of comments on mflix
 *               title:
 *                 type: string
 *                 description: The title of the movie
 *               released:
 *                 type: string
 *                 description: The release date of the movie
 *               directors:
 *                 type: array
 *                 items:
 *                   type: string
 *                 description: The directors of the movie
 *               year:
 *                 type: string
 *                 description: The year of the movie
 *     responses:
 *       201:
 *         description: Movie successfully added
 *       400:
 *         description: Bad Request. The request body is missing required fields or contains invalid data.
 *       500:
 *         description: Internal Server Error. An error occurred while processing the request.
 */

export default async function handler(req, res) {
    switch (req.method) {
        case "POST":
            const movieToInsert = req.body;
            if (!movieToInsert.title) {
                return json({ status: 400, error: "Bad Request", message: "Le titre est requis" });
            }
            if (movieToInsert._id) {
                return json({status: 400, error: "Bad Request", message: "L'attribut _id ne doit pas être fourni" });
            }
            const result = await OrmService.insert(Config.collections.movies, movieToInsert)
            res.json({ status: 201, data: result});
            break;
        case "GET":
            let { page } = req.query;
            if(!page || parseInt(page) == 0) {
                page = 1
            }
            const movies = await OrmService.find(Config.collections.movies, {}, page);
            res.json({ status: 200, data: movies, page: page });
            break;
        default :
            res.json({ status: 405, data: "Method not allowed" });
            break;
    }
}
