import React, { useState, useEffect } from 'react';
import axios from 'axios';
import MoviesComponent from './movies';
import MovieComponent from './movie';

const IndexPage = () => {
  return (
    <div>
      <h1>MFLIX API - test sur la table movies</h1>
      <MoviesComponent/>
      <hr />
      <MovieComponent/>
      
    </div>
  );
};

export default IndexPage;
