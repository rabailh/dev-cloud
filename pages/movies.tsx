import React, { useState, useEffect } from 'react';
import axios from 'axios';

/*
* Component for endpoints from /movie
* get of movies and post
*/
const MoviesComponent = () => {
  
  const [movies, setMovies] = useState([]);
  const [selectedMovie, setSelectedMovie] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [deleteResult, setDeleteResult] = useState(null);


  useEffect(() => {
    fetchMovies();
  }, [currentPage]);

  const fetchMovies = async () => {
    try {
      const response = await axios.get(`/api/movies?page=${currentPage}`);
      setMovies(response.data.data);
    } catch (error) {
      console.error('Error fetching movies:', error);
    }
  };

  const handleSelectMovie = async (movieId) => {
    try {
      const response = await axios.get(`/api/movie/${movieId}`);
      setSelectedMovie(response.data.data.movie);
    } catch (error) {
      console.error('Error fetching movie details:', error);
    }
  };

  const handlePrevPage = () => {
    setCurrentPage(prevPage => Math.max(prevPage - 1, 1));
  };

  const handleNextPage = () => {
    setCurrentPage(prevPage => prevPage + 1);
  };

  const handleDeleteMovieById = async (movieId) => {
    try {
      const response = await axios.delete(`/api/movie/${movieId}`);
      setDeleteResult({ success: true, message: 'Film supprimé avec succès.' });
    } catch (error) {
      console.error('Error deleting movie:', error);
      setDeleteResult({ success: false, message: 'Erreur lors de la suppression du film.' });
    }
  };
    
    return (
        <div>
          <hr />
          <h2>Liste des films</h2>
          <ul>
            {movies.map((movie, index) => (
              <li key={movie._id} onClick={() => handleSelectMovie(movie._id)}>
                {(index + 1) + (currentPage - 1) * 10} - <a href="#">{movie.title}</a>
              </li>
            ))}
          </ul>
          <button onClick={handlePrevPage}>Page précédente</button>
          <span> Page {currentPage} </span>
          <button onClick={handleNextPage}>Page suivante</button>
          <hr />
          <h2>Film selectionné :</h2>
          <p>{selectedMovie ? '' : `aucun film selectionné`} </p>
          {selectedMovie && (
            <ul>
              <li><h2>{selectedMovie.title}</h2></li>
              <li><strong>_id :</strong> {selectedMovie._id}</li>
              <li><strong>plot :</strong> {selectedMovie.plot}</li>
              <li><strong>genres :</strong> {selectedMovie.genres.join(', ')}</li>
              <li><strong>runtime :</strong> {selectedMovie.runtime}</li>
              <li><strong>cast :</strong> {selectedMovie.cast.join(', ')}</li>
              <li><strong>nombre de commentaire mflix :</strong> {selectedMovie.num_mflix_comments}</li>
              <li><strong>released :</strong> {selectedMovie.released}</li>
              <li><strong>directors :</strong> {selectedMovie.directors.join(', ')}</li>
              <li><strong>year :</strong> {selectedMovie.year}</li>
            </ul>
          )}
          <hr />
          <h2>Suppression</h2>
          {selectedMovie && (
            <button onClick={() => handleDeleteMovieById(selectedMovie._id)}>Supprimer le film sélectionné</button>
          )}
          {deleteResult && (
            <div className={`update-message ${deleteResult.success ? 'success' : 'error'}`}>
              {deleteResult.message}
            </div>
          )}
          <style>
        {
          `
          body {
            font-family: Arial, sans-serif;
            padding: 20px;
          }
          
          h1 {
            text-align: center;
            margin-bottom: 20px;
          }
          
          h2 {
            margin-top: 20px;
          }
          
          hr {
            margin: 20px 0;
            border: none;
            border-top: 1px solid #ccc;
          }
          
          ul {
            list-style-type: none;
            padding: 0;
          }
          
          li {
            margin-bottom: 10px;
          }
          
          button {
            background-color: #007bff;
            color: #fff;
            border: none;
            padding: 8px 16px;
            cursor: pointer;
            border-radius: 4px;
            margin-top : 5px;
            transition: background-color 0.3s ease;
          }
          
          button:hover {
            background-color: #0056b3;
          }
          
          input{
            padding: 8px;
            border-radius: 4px;
            border: 1px solid #ccc;
            margin-right : 10px;
            width: 250px;
            box-sizing: border-box;
          }
          
          form {
            margin-bottom: 20px;
          }
          
          form label {
            display: block;
            margin-bottom: 5px;
          }
          
          `
        }
      </style>
        </div>
      );
};

export default MoviesComponent;
