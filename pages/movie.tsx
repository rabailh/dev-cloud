import React, { useState, useEffect } from 'react';
import axios from 'axios';

/*
* Component for endpoints from /movie
* get by id, put and delete
*/
const MovieComponent = () => {

    const [formData, setFormData] = useState({
        title: '',
        movieId: ''
    });
    const [result, setResult] = useState(null);
    const [searchResult, setSearchResult] = useState(null);
    const [updateResult, setUpdateResult] = useState(null);



    const handleAddMovie = async (e : React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            const response = await axios.post('/api/movies', { title: formData.title });
            const insertedId = response.data.data.insertedId;
            setResult({ success: true, id: insertedId, message: 'Ajout du film réussi avec succès. ' });
        } catch (error) {
            setResult({ success: false, message: 'Erreur lors de l\'ajout du film.' });
            console.error('Error adding movie:', error);
        }
    };

    const handleSearchMovieById = async (e) => {
        if (e && typeof e.preventDefault === 'function') {
            e.preventDefault();
        }
        try {
            const response = await axios.get(`/api/movie/${formData.movieId}`);
            setSearchResult(response.data.data.movie);
        } catch (error) {
            console.error('Error searching movie by ID:', error);
            setSearchResult(null);
        }
    };

    const handleUpdateMovie = async (e : React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            const updateData = {};
            // Select infos not empty
            Object.entries(formData).forEach(([key, value]) => {
                if (value !== '') {
                    updateData[key] = value;
                }
            });

            // Assert that there is new infos to set
            if (Object.keys(updateData).length === 0) {
                return;
            }

            const response = await axios.put(`/api/movie/${searchResult._id}`, updateData);

            // Update selected movie with new infos
            handleSearchMovieById(searchResult._id);
            setUpdateResult({ success: true, message: 'Film mis à jour avec succès.' });
        } catch (error) {
            console.error('Error updating movie:', error);
            setUpdateResult({ success: false, message: 'Erreur lors de la mise à jour du film.' });
        }
    };

    const handleInputChange = (e : React.FormEvent<HTMLFormElement>) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    return (
        <div>
            <h2>Ajouter un film</h2>
            <form onSubmit={handleAddMovie}>
                <input type="text" name="title" placeholder="Title" value={formData.title} onChange={handleInputChange} required />
                <button type="submit">Valider</button>
            </form>
            {result && (
                <div className={`update-message ${result.success ? 'success' : 'error'}`}>
                    {result.message}{result.success ? ("id : " + (result.id ?? '')) : ''}
                </div>
            )}

            <hr />
            <h2>Rechercher un film par ID</h2>
            <form onSubmit={handleSearchMovieById}>
                <input type="text" name="movieId" placeholder="ID du film" value={formData.movieId} onChange={handleInputChange} required />
                <button type="submit">Rechercher</button>
            </form>
            {searchResult && (
                <div>
                    <h3>Résultat de la recherche :</h3>
                    <ul>
                        <li><h2>{searchResult.title ?? 'vide'}</h2></li>
                        <li><strong>_id :</strong> {searchResult._id ?? 'vide'}</li>
                        <li><strong>plot :</strong> {searchResult.plot ?? 'vide'}</li>
                        <li><strong>genres :</strong> {searchResult.genres ? searchResult.genres.join(', ') : 'vide'}</li>
                        <li><strong>runtime :</strong> {searchResult.runtime ?? 'vide'}</li>
                        <li><strong>cast :</strong> {searchResult.cast ? searchResult.cast.join(', ') : 'vide'}</li>
                        <li><strong>nombre de commentaire mflix :</strong> {searchResult.num_mflix_comments ?? 'vide'}</li>
                        <li><strong>released :</strong> {searchResult.released ?? 'vide'}</li>
                        <li><strong>directors :</strong> {searchResult.directors ? searchResult.directors.join(', ') : 'vide'}</li>
                        <li><strong>year :</strong> {searchResult.year ?? 'vide'}</li>
                    </ul>
                </div>
            )}
            <hr />
            <h2>Modifier le film sélectionné</h2>
            {searchResult && (
                <div>
                    <form onSubmit={handleUpdateMovie}>
                        <ul>
                            <li>
                                <label htmlFor="updateTitle">Titre :</label>
                                <input type="text" id="updateTitle" name="title" value={formData.title || searchResult.title} onChange={handleInputChange} />
                            </li><li>
                                <label htmlFor="updatePlot">Plot :</label>
                                <input type="text" id="updatePlot" name="plot" value={formData.plot ?? searchResult.plot} onChange={handleInputChange} />
                            </li><li>
                                <label htmlFor="updateGenres">Genres :</label>
                                <input type="text" id="updateGenres" name="genres" value={formData.genres ?? (searchResult.genres ? searchResult.genres.join(', ') : '')} onChange={handleInputChange} />
                            </li><li>
                                <label htmlFor="updateRuntime">Runtime :</label>
                                <input type="text" id="updateRuntime" name="runtime" value={formData.runtime ?? searchResult.runtime} onChange={handleInputChange} />
                            </li><li>
                                <label htmlFor="updateCast">Cast :</label>
                                <input type="text" id="updateCast" name="cast" value={formData.cast ?? (searchResult.cast ? searchResult.cast.join(', ') : '')} onChange={handleInputChange} />
                            </li><li>
                                <label htmlFor="updateNumComments">Nombre de commentaires mflix :</label>
                                <input type="text" id="updateNumComments" name="num_mflix_comments" value={formData.num_mflix_comments ?? (searchResult.num_mflix_comments !== undefined ? searchResult.num_mflix_comments : '')} onChange={handleInputChange} />
                            </li><li>
                                <label htmlFor="updateReleased">Released :</label>
                                <input type="text" id="updateReleased" name="released" value={formData.released ?? searchResult.released} onChange={handleInputChange} />
                            </li><li>
                                <label htmlFor="updateDirectors">Directors :</label>
                                <input type="text" id="updateDirectors" name="directors" value={formData.directors ?? (searchResult.directors ? searchResult.directors.join(', ') : '')} onChange={handleInputChange} />
                            </li><li>
                                <label htmlFor="updateYear">Year :</label>
                                <input type="text" id="updateYear" name="year" value={formData.year ?? searchResult.year} onChange={handleInputChange} />
                            </li>
                            <button type="submit">Modifier</button>
                        </ul>
                    </form>
                </div>
            )}

            {updateResult && (
                <div className={`update-message ${updateResult.success ? 'success' : 'error'}`}>
                    {updateResult.message}
                </div>
            )}
            <style>
        {
          `
          body {
            font-family: Arial, sans-serif;
            padding: 20px;
          }
          
          
          h2 {
            margin-top: 20px;
          }
          
          hr {
            margin: 20px 0;
            border: none;
            border-top: 1px solid #ccc;
          }
          
          ul {
            list-style-type: none;
            padding: 0;
          }
          
          li {
            margin-bottom: 10px;
          }
          
          button {
            background-color: #007bff;
            color: #fff;
            border: none;
            padding: 8px 16px;
            cursor: pointer;
            border-radius: 4px;
            margin-top : 5px;
            transition: background-color 0.3s ease;
          }
          
          button:hover {
            background-color: #0056b3;
          }
          
          input{
            padding: 8px;
            border-radius: 4px;
            border: 1px solid #ccc;
            margin-right : 10px;
            width: 250px;
            box-sizing: border-box;
          }
          
          form {
            margin-bottom: 20px;
          }
          
          form label {
            display: block;
            margin-bottom: 5px;
          }
          
          .search-result {
            background-color: #f9f9f9;
            border: 1px solid #ccc;
            border-radius: 4px;
            padding: 10px;
            margin-bottom: 20px;
          }
          
          .search-result h3 {
            margin-top: 0;
          }
          
          .update-message {
            margin-top: 20px;
            padding: 10px;
            border-radius: 4px;
          }
          
          .update-message.success {
            background-color: #d4edda;
            border-color: #c3e6cb;
            color: #155724;
          }
          
          .update-message.error {
            background-color: #f8d7da;
            border-color: #f5c6cb;
            color: #721c24;
          }
          `
        }
      </style>
        </div>
    );
};

export default MovieComponent;